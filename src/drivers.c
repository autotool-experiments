#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#include "drivers.h"

/*
 * The runtime registration approach. Assumes that registering drivers
 * provide the list node storage, and that this is the only use of this
 * memory.
 */

static struct driver_list_cons_node *driver_list_cons_head;
static struct driver_list_cons_node **driver_list_cons_tail = &driver_list_cons_head;
static size_t driver_list_cons_size;

void register_driver_cons(struct driver_list_cons_node *node) {

	if (!node)
		return;
	node->next = NULL;

	*driver_list_cons_tail = node;
	driver_list_cons_tail = &node->next;
	driver_list_cons_size++;
}

static void iterate_drivers_cons(void)
{
	struct driver_list_cons_node *node;
	const struct driver_details *d;

	printf("items:\n");
	for (node = driver_list_cons_head; node; node = node->next) {
		d = node->driver;
		if (!d)
			continue;
		printf(" %p %d %s %p\n", d,
			d->identification, d->label, d->callback);
	}
	printf("list end\n");
	printf("item count: %zu\n", driver_list_cons_size);
}

static void execute_drivers_cons(void)
{
	struct driver_list_cons_node *node;
	const struct driver_details *d;

	for (node = driver_list_cons_head; node; node = node->next) {
		d = node->driver;
		if (!d)
			continue;
		d->callback();
	}
}

extern const struct driver_details *const sr_driver_list_sect__start;

static void iterate_drivers_sect(void)
{
	const struct driver_details *const *p;
	const struct driver_details *d;
	size_t count;

	printf("items:\n");
	count = 0;
	for (p = &sr_driver_list_sect__start; *p; p++) {
		d = *p;
		count++;
		printf(" %p %d %s %p\n", d,
			d->identification, d->label, d->callback);
	}
	printf("list end\n");
	printf("item count: %zu\n", count);
}

static void execute_drivers_sect(void)
{
	const struct driver_details *const *p;
	const struct driver_details *d;

	for (p = &sr_driver_list_sect__start; *p; p++) {
		d = *p;
		d->callback();
	}
}

void iterate_drivers(void)
{
	printf("iteration, constructors:\n");
	iterate_drivers_cons();
	printf("iteration, linker section:\n");
	iterate_drivers_sect();
}

void execute_drivers(void)
{
	printf("execution, constructors:\n");
	execute_drivers_cons();
	printf("execution, linker section:\n");
	execute_drivers_sect();
}
