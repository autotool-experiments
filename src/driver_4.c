#include "config.h"

#include <stdio.h>
#include "drivers.h"

#if defined WITH_DRIVER_4
static void local_func(void)
{
	printf("driver4, do something\n");
}
DECLARE_DRIVER(driver4, 4, local_func);
#endif
