#include "config.h"

#include <stdio.h>
#include "drivers.h"

#if defined WITH_DRIVER_5
static void local_func(void)
{
	printf("driver5, do something\n");
}
DECLARE_DRIVER(driver5, 5, local_func);
#endif
