#ifndef DRIVERS_H
#define DRIVERS_H

/*
 * public API {{{
 *
 * This is the public API. To iterate driver list items and access their
 * respective properties.
 */

void iterate_drivers(void);
void execute_drivers(void);

struct driver_details {
	int identification;
	const char *label;
	void (*callback)(void);
};

/* }}} */

/*
 * implementation details {{{
 *
 * These are internal details. Though individual drivers need to use
 * the DECLARE_DRIVER() declaration helper, they shall not care about
 * internal details of this implementation. Consider it opaque.
 */

/*
 * driver description {{{
 *
 * This is common across all alternative approaches. A plain structure
 * holds the driver's details. The structure is kept with read-only
 * variables as usual. The magic is in the driver _list_ creation which
 * happens elsewhere.
 */

#define DECLARE_DRIVER_DESC(name, id, func) \
	static const struct driver_details driver_ ## name ## _detail = { \
		.identification = (id), \
		.label = #name, \
		.callback = (func), \
	}; \
	/* end of DECLARE_DRIVER_DESC() */

/* }}} */

/*
 * constructor attribute {{{
 *
 * Declares static nodes, which get linked into a list at runtime before
 * main() executes, by running constructor code per registered driver.
 */

struct driver_list_cons_node {
	const struct driver_details *driver;
	struct driver_list_cons_node *next;
};
void register_driver_cons(struct driver_list_cons_node *node);

/* TODO
 * - Needs per-platform declaration of the constructor attribute?
 *   See the glib header for an implementation.
 */
#define ATTR_CONS __attribute__((constructor))

#define DECLARE_DRIVER_CONS(name) \
	static struct driver_list_cons_node driver_ ## name ## _cons_node = { \
		.driver = &driver_ ## name ## _detail, \
	}; \
	static void driver_ ## name ## _cons_register(void) ATTR_CONS; \
	static void driver_ ## name ## _cons_register(void) { \
		register_driver_cons(&driver_ ## name ## _cons_node); \
	} \
	/* end of DECLARE_DRIVER_CONS() */

/* }}} */

/*
 * TODO
 * - Introduce more alternative declarations. Re-introduce the linker
 *   section approach, linker scripts do seem to take effect although
 *   their command line args unexpectedly get split.
 */

/*
 * linker section {{{
 *
 * Puts a list of pointers to driver descriptors into a linker section.
 * The fact that the section exclusively collects pointers allows to
 * use it as an array. A linker script will ensure the termination.
 */

/* taken from libsigrok-internal.h {{{ */

#ifdef __APPLE__
#define SR_DRIVER_LIST_SECTION "__DATA,__sr_driver_list"
#else
#define SR_DRIVER_LIST_SECTION "__sr_driver_list"
#endif

#if !defined SR_DRIVER_LIST_NOREORDER && defined __has_attribute
#if __has_attribute(no_reorder)
#define SR_DRIVER_LIST_NOREORDER __attribute__((no_reorder))
#endif
#endif
#if !defined SR_DRIVER_LIST_NOREORDER
#define SR_DRIVER_LIST_NOREORDER /* EMPTY */
#endif

/* TODO SR_PRIV */

/* }}} end of libsigrok-internal.h */

#define ATTR_SECT __attribute__((section(SR_DRIVER_LIST_SECTION), used, \
	aligned(sizeof(struct driver_details *))))

#define DECLARE_DRIVER_SECT(name) \
	static const struct driver_details *driver_ ## name ## _sect_items[] \
	SR_DRIVER_LIST_NOREORDER \
	__attribute__((section(SR_DRIVER_LIST_SECTION), \
		aligned(sizeof(struct driver_details *)), used)) \
	= { &driver_ ## name ## _detail, }; \
	/* end of DECLARE_DRIVER_SECT() */

/* }}} */

#define DECLARE_DRIVER(name, id, func) \
	DECLARE_DRIVER_DESC(name, id, func); \
	DECLARE_DRIVER_CONS(name); \
	DECLARE_DRIVER_SECT(name); \
	/* TODO List more alternatives here. */ \
	/* end of DECLARE_DRIVER() */

/* }}} */

#endif /* DRIVERS_H */

/*
 * vim:foldmethod=marker:
 */
