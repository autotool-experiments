#include "config.h"

#include <stdio.h>
#include "drivers.h"

#if defined WITH_DRIVER_2
static void local_func(void)
{
	printf("driver2, do something\n");
}
DECLARE_DRIVER(driver2, 2, local_func);
#endif
