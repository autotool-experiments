#include "config.h"

#include <stdio.h>
#include "drivers.h"

#if defined WITH_DRIVER_3
static void local_func(void)
{
	printf("driver3, do something\n");
}
DECLARE_DRIVER(driver3, 3, local_func);
#endif
