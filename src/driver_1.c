#include "config.h"

#include <stdio.h>
#include "drivers.h"

#if defined WITH_DRIVER_1
static void local_func(void)
{
	printf("driver1, do something\n");
}
DECLARE_DRIVER(driver1, 1, local_func);
#endif
