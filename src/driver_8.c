#include "config.h"

#include <stdio.h>
#include "drivers.h"

#if defined WITH_DRIVER_8
static void local_func_81(void)
{
	printf("driver81, do something\n");
}
DECLARE_DRIVER(driver81, 81, local_func_81);

static void local_func_82(void)
{
	printf("driver82, do something\n");
}
DECLARE_DRIVER(driver82, 82, local_func_82);

static void local_func_83(void)
{
	printf("driver83, do something\n");
}
DECLARE_DRIVER(driver83, 83, local_func_83);
#endif
