#include "config.h"

#include <stdio.h>
#include "drivers.h"

#if defined WITH_DRIVER_6
static void local_func_6(void)
{
	printf("driver6, do something\n");
}
DECLARE_DRIVER(driver6, 6, local_func_6);
#endif

#if defined WITH_DRIVER_7
static void local_func_7(void)
{
	printf("driver7, do something\n");
}
DECLARE_DRIVER(driver7, 7, local_func_7);
#endif
